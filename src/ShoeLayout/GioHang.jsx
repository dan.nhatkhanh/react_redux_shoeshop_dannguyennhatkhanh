import React, { Component } from "react";
import { connect } from "react-redux";
import {
  DELETE_CART,
  GIAM_SO_LUONG,
  TANG_SO_LUONG,
} from "./redux/constant/shoeConstants";

class GioHang extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td className="d-flex justify-content-center">
            {" "}
            <img
              className="img-fluid"
              src={item.image}
              alt="shoe"
              style={{ width: 50 }}
            />
          </td>
          <td>
            <button
              onClick={() => this.props.handleReduceProduct(item)}
              className="btn btn-primary"
            >
              -
            </button>
            <span className="px-3 font-weight-bold">{item.soLuong}</span>
            <button
              onClick={() => this.props.handlePlusProduct(item)}
              className="btn btn-success"
            >
              +
            </button>
          </td>

          <td>
            <button
              onClick={() => this.props.handleRemoveProduct(item)}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container p-5">
        <table className="table table-bordered text-left ">
          <thead>
            <tr>
              <th>Tên</th>
              <th>Số lượng</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
            </tr>
          </thead>

          <tbody>{this.renderTbody()}</tbody>
        </table>

        {this.props.cart.length == 0 && (
          <p className="mt-5 text-center">Chưa có sản phẩm trong giỏ hàng</p>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleReduceProduct: (value) => {
      dispatch({
        type: GIAM_SO_LUONG,
        payload: value,
      });
    },

    handlePlusProduct: (value) => {
      dispatch({
        type: TANG_SO_LUONG,
        payload: value,
      });
    },

    handleRemoveProduct: (value) => {
      dispatch({
        type: DELETE_CART,
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GioHang);
