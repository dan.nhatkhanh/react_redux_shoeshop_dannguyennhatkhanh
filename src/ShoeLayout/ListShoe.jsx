import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          {this.props.shoeArr.map((item, index) => {
            return (
              <div key={index.toString() + item.id} className="col-3">
                <ItemShoe detail={item} />
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
