import { dataArr } from "../../data_shoe";
import {
  ADD_TO_CART,
  DELETE_CART,
  GIAM_SO_LUONG,
  TANG_SO_LUONG,
  XEM_CHI_TIET,
} from "../constant/shoeConstants";

let initialSate = {
  shoeArr: dataArr,
  detailShoe: dataArr[0],
  cart: [],
};

export let shoeReducer = (state = initialSate, { type, payload }) => {
  switch (type) {
    case XEM_CHI_TIET: {
      state.detailShoe = payload;

      return { ...state };
    }

    case ADD_TO_CART: {
      let cloneCart = [...state.cart];

      state.detailShoe = payload;

      let index = state.cart.findIndex((item) => {
        return item.id == payload.id;
      });

      if (index == -1) {
        let numberProduct = { ...payload, soLuong: 1 };
        console.log("numberProduct: ", numberProduct);
        cloneCart.push(numberProduct);
      } else {
        cloneCart[index].soLuong++;
      }

      state.cart = cloneCart;
      console.log("state.cart: ", state.cart);

      return { ...state };
    }

    case GIAM_SO_LUONG: {
      let cloneCart = [...state.cart];

      state.detailShoe = payload;

      let index = state.cart.findIndex((item) => {
        return item.id == payload.id;
      });

      if (cloneCart[index].soLuong > 1) {
        cloneCart[index].soLuong--;
      }

      console.log("cloneCart[index].soLuong: ", cloneCart[index].soLuong);

      state.cart = cloneCart;

      return { ...state };
    }

    case TANG_SO_LUONG: {
      let cloneCart = [...state.cart];

      state.detailShoe = payload;

      let index = state.cart.findIndex((item) => {
        return item.id == payload.id;
      });

      cloneCart[index].soLuong++;

      state.cart = cloneCart;

      return { ...state };
    }

    case DELETE_CART: {
      let cloneCart = [...state.cart];

      state.detailShoe = payload;

      let index = state.cart.findIndex((item) => {
        return item.id == payload.id;
      });

      if (index !== -1) {
        cloneCart.splice(cloneCart[index], 1);
      }

      state.cart = cloneCart;

      return { ...state };
    }

    default:
      return state;
  }
};
