import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART, XEM_CHI_TIET } from "./redux/constant/shoeConstants";

class ItemShoe extends Component {
  render() {
    let { name, image } = this.props.detail;

    return (
      <div className="card" style={{ width: "18rem" }}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.detail);
            }}
            className="btn btn-secondary mr-3"
          >
            Add to cart
          </button>

          <button
            onClick={() => {
              this.props.handleViewDetail(this.props.detail);
            }}
            className="btn btn-warning"
          >
            More
          </button>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleViewDetail: (value) => {
      dispatch({
        type: XEM_CHI_TIET,
        payload: value,
      });
    },

    handleAddToCart: (value) => {
      dispatch({
        type: ADD_TO_CART,
        payload: value,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
